package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {

    BookRepository bookRepository;
    private MutableLiveData<Book> result = new MutableLiveData<>();


    public DetailViewModel (@NonNull Application application) {
        super(application);
        bookRepository = new BookRepository(application);
        result = bookRepository.getSelectedBook();
    }

    public MutableLiveData<Book> getBook()
    {
        return result;
    }

    public void updateBook(Book book){ bookRepository.updateBook(book); }

    public void setBook(long id){
        bookRepository.findBook(id);
        this.result = bookRepository.getSelectedBook();
    }

}
